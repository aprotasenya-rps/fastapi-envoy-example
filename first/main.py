from fastapi import FastAPI, Header, Depends
import requests
from typing import Callable

X_REQUEST_ID_HEADER = 'X-Request-Id'

class Requests(object):
    def __init__(self):
        pass

    def __call__(self, *, x_request_id: str = Header(None)):
        def get(url):
            return requests.get(url, headers={X_REQUEST_ID_HEADER: x_request_id})
        return get

app = FastAPI()

req = Requests()

@app.get('/')
async def index(client: Callable = Depends(req)):
    return client('http://second-service').json()
