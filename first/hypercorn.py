bind="0.0.0.0:8000"
accesslog="-"
access_log_format='''
{
    "service": "%({SERVICE_NAME}e)s",
    "time": "%(t)s",
    "request": "%(r)s",
    "http_status_code": "%(s)s",
    "http_request_url": "%(U)s",
    "http_query_string": "%(q)s",
    "http_verb": "%(m)s",
    "http_version": "%(H)s",
    "x-request-id": "%({x-request-id}i)s"
}'''
workers=4
